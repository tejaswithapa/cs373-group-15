from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.routers import states, supportgroups, professionals

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://adhdallyus.site",
    "https://adhdallyus.site",
    "http://www.adhdallyus.site",
    "https://www.adhdallyus.site",
    "http://api.adhdallyus.site",
    "https://api.adhdallyus.site",
    "http://texastrauma.support",
    "https://texastrauma.support",
    "http://www.texastrauma.support",
    "https://www.texastrauma.support",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(states.router)
app.include_router(professionals.router)
app.include_router(supportgroups.router)


@app.get("/")
async def root():
    return {"message": "Hello from ADHD Ally API!"}

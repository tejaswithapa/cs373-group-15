# import os
# from sqlalchemy import create_engine
from fastapi import Query
from typing import Annotated
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

# supabase:
# username: namanarora@utexas.edu
# password: ADHDAlly123!
# database password: 5yXCgGQ60mcN2H6t

# url: str = os.environ.get("SUPABASE_URL")
# key: str = os.environ.get("SUPABASE_KEY")

db_pass = "5yXCgGQ60mcN2H6t"

db_url = f"postgresql+asyncpg://postgres.wljmzzkkdvkbfouabpra:{db_pass}@aws-0-us-west-1.pooler.supabase.com:5432/postgres"

engine = create_async_engine(db_url, echo=True)


async def get_db():
    async with AsyncSession(engine) as session:
        yield session


async def get_params(
    page: Annotated[int, Query(title="Page number", ge=1)] = 1,
    limit: Annotated[int, Query(title="Limit", ge=1)] = 15,
):
    return {"page": page, "limit": limit, "offset": limit * (page - 1)}
    # return (page, limit, (limit * (page - 1)))

import unittest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from app.routers import states, supportgroups, professionals


class TestAPIEndpoints(unittest.TestCase):
    def setUp(self):
        # Setup a test client for the FastAPI application
        
        app = FastAPI()
        
        app.include_router(states.router)
        app.include_router(professionals.router)
        app.include_router(supportgroups.router)
        
        self.client = TestClient(app)


    # Unit Test 1
    async def test_get_all_professionals(self):
        with self.client:
            response = self.client.get("/professionals/")
            self.assertEqual(response.status_code, 200)

    # Unit Test 2
    async def test_get_professionals_metadata(self):
        with self.client:
            response = self.client.get("/professionals/metadata")
            self.assertEqual(response.status_code, 200)

    # Unit Test 3
    async def test_get_professional_by_id(self):
        with self.client:
            valid_uuid = 'acb3007b-2fc7-49b2-a834-ad73747ccff1'
            response = self.client.get(f"/professionals/{valid_uuid}")
            self.assertIn(response.status_code, (200, 404))

    # Unit Test 4
    async def test_get_professionals_by_state(self):
        with self.client:
            response = self.client.get("/professionals/state/Texas")
            self.assertEqual(response.status_code, 200)

    # Unit Test 5
    async def test_get_all_supportgroups(self):
        with self.client:
            response = self.client.get("/supportgroups/")
            self.assertEqual(response.status_code, 200)
            
    # Unit Test 6
    async def test_get_supportgroups_metadata(self):
        with self.client:
            response = self.client.get("/supportgroups/metadata")
            self.assertEqual(response.status_code, 200)

    # Unit Test 7
    async def test_get_supportgroup_by_id(self):
        with self.client:
            valid_uuid = '593e4fd7-a653-43e8-9790-796efd5b663d'  
            response = self.client.get(f"/supportgroups/{valid_uuid}")
            self.assertIn(response.status_code, (200, 404))

    # Unit Test 8
    async def test_get_supportgroups_by_state(self):
        with self.client:
            response = self.client.get("/supportgroups/state/Texas")
            self.assertEqual(response.status_code, 200)

    # Unit Test 9
    async def test_get_all_states(self):
        with self.client:
            response = self.client.get("/states/?limit=10&page=1")
            self.assertEqual(response.status_code, 200)

    # Unit Test 10
    async def test_get_states_metadata(self):
        with self.client:
            response = self.client.get("/states/metadata")
            self.assertEqual(response.status_code, 200)

    # Unit Test 11
    async def test_get_state_by_name(self):
        with self.client:
            state_name = 'Texas'  
            response = self.client.get(f"/states/{state_name}")
            self.assertIn(response.status_code, (200, 404))
      
    # Search, Sort, Filter
    # Unit Test 12
    async def test_states_search(self):
        with self.client:
            response = self.client.get("/states?search=Ala")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json["count"], 2)
   
    # Unit Test 13
    async def test_professionals_search(self):
        with self.client:
            response = self.client.get("/professionals?search=Atlanta")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json["count"], 2)

    # Unit Test 14
    async def test_states_sort(self):
        with self.client:
            response = self.client.get("/states?sort=Total+Population&order=desc")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json["count"], 50)
            self.assertEqual(response.json["data"][0]["name"], "California")

    # Unit Test 15
    async def test_supportgroups_sort(self):
        with self.client:
            response = self.client.get("/supportgroups?sort=Email&order=asc")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json["count"], 102)
            self.assertEqual(response.json["data"][0]["name"], "West Suburban Adult Satellite of CHADD")

if __name__ == '__main__':
    unittest.main()

# get git config
config:
	git config -l

all:

# install dependencies
dep:
	cd ./frontend && npm install

dev:
	cd ./frontend && npm run dev

clean:
	rm -rf ./build

clean-dep:
	rm -rf ./node_modules

scrub:
	make clean
	make clean-dep

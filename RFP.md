# Request for Proposal

**Group Number:** 15

**Team Members:** Abdallah Al-Sukhni, Haoyu Fan, Naman Arora, Tejaswi Thapa, Faith Nguyen

**Project Name:** ADHD Ally US

**Proposed Project:** ADHD Ally US aims to support children aged 3 to 17 years with ADHD in the United States. We accomplish this by connecting these individuals with ADHD support groups and professionals. We also provide state-level statistics about ADHD to raise awareness about the condition.

**Data Sources:**

1. [State-Level Estimates of ADHD Diagnosis and Treatment](https://journals.sagepub.com/eprint/A4CQI4TZJAJRIMIPYKET/full)
2. [CHADD Affiliates](https://chadd.org/affiliate-locator/)
3. [ADDA Professional Directory](https://add.org/professional-directory/)
4. [Google Photos API](https://developers.google.com/photos)
5. RESTful API: [Google Maps API](https://developers.google.com/maps)
6. [List of U.S. States by Population](https://www.britannica.com/topic/largest-U-S-state-by-population)

**Models:**

1. ADHD Support Groups
2. ADHD Professionals
3. States

**Estimated of Number of Instances:**

1. ADHD Support Groups: 108
2. ADHD Professionals : 383
3. States: 50

**Model Attributes:**

1. ADHD Support Groups
    - Name
    - Contact Person
    - Location
    - State
    - Phone
    - Email
2. ADHD Professionals
    - Name
    - Title
    - Organization
    - Location
    - State
    - Phone
3. States
    - Name
    - Total population
    - Population ever received ADHD diagnosis
    - Population has current ADHD
    - Percentage currently taking ADHD medication
    - Percentage received behavioral treatment for ADHD in past 12 months

**Connecting Models:**
Instances of ADHD Support Groups and instances of ADHD Professionals are connected to each other by proximity, in other words, connected by instances of States.

**Media Types:**

1. ADHD Support Groups
    - Images
    - Maps
    - Text
2. ADHD Professionals
    - Images
    - Maps
    - Text
3. States
    - Images
    - Maps
    - Text

**Questions Our Site Answers:**

1. What support groups are available for me as a child with ADHD?
2. What professionals can I connect with as a child with ADHD?
3. What are some statistics about ADHD in the state that I live in?

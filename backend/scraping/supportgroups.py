from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep
import json
import re  # Importing the regex module for pattern matching

def extract_data(soup):
    data = {
        "name": "",
        "contact_person": "Laurie Kulikosky",  # Default contact person
        "location": "",
        "state": "",
        "phone": "301-306-7070",  # Default phone number
        "email": "customer_service@chadd.org"  # Default email address
    }

    # Extract Name
    name_tag = soup.find('h2', class_='font_2')
    if name_tag and name_tag.a:
        data['name'] = name_tag.a.text.strip()

    # Attempt to Extract Contact Person
    contact_tags = soup.find_all('p', class_='font_8')
    for tag in contact_tags:
        if "Contact:" in tag.text:
            data['contact_person'] = tag.text.split("Contact:")[-1].strip()
            break

    # Attempt to Extract Address and State
    address_tag = soup.find('p', text=lambda x: "Address:" in x if x else False)
    if address_tag:
        next_p_tag = address_tag.find_next_sibling('p')
        if next_p_tag:
            data['location'] = ' '.join(next_p_tag.stripped_strings).replace('<br>', ', ')
            # Attempt to extract state from the location
            match = re.search(r',\s*([A-Z]{2})\s*\d{5}', data['location'])
            if match:
                data['state'] = match.group(1)

    # Attempt to Extract Phone
    phone_tag = soup.find('a', href=lambda x: x and x.startswith('tel:'))
    if phone_tag:
        data['phone'] = phone_tag.text.strip()

    # Attempt to Extract Email
    email_tag = soup.find('a', href=lambda x: x and x.startswith('mailto:'))
    if email_tag:
        data['email'] = email_tag.text.strip()

    return data


def scrape_individual_chapter(driver, url):
    driver.get(url)
    sleep(3)  # Adjust based on page load times
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    return extract_data(soup)

def main():
    driver = webdriver.Chrome()  # Ensure chromedriver is installed and in PATH
    
    # Scraping chapter URLs from the main page (adjust URL as needed)
    main_url = 'https://chadd.org/affiliate-locator/'
    driver.get(main_url)
    sleep(3)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    chapter_urls = [a['href'] for a in soup.find_all('a', href=True) if 'chapter' in a['href']]

    # Visit each chapter URL and scrape detailed information
    all_chapter_data = [] 
    for url in chapter_urls:
        chapter_data = scrape_individual_chapter(driver, url)
        all_chapter_data.append(chapter_data)
        print(f"Scraped: {url}")  # Optional: for progress tracking
        sleep(1)  # Be respectful to the server

    # Export scraped data to a JSON file
    with open('chapter_details.json', 'w', encoding='utf-8') as f:
        json.dump(all_chapter_data, f, indent=4)

    print("Scraping complete. Data saved to chapter_details.json.")
    driver.quit()

if __name__ == "__main__":
    main()

from bs4 import BeautifulSoup
import re
import json
import requests

html_code1 = """
<div class="table-wrap" tabindex="0"><table><thead><tr><td>&nbsp;</td><th colspan="4"><span>Ever received ADHD diagnosis</span></th><th colspan="4"><span>Has current ADHD</span></th></tr><tr><td>&nbsp;</td><th>Weighted population estimate</th><th>Weighted population estimate 95% CI</th><th>Weighted %</th><th>95% CI</th><th>Weighted population estimate</th><th>Weighted population estimate 95% CI</th><th>Weighted %</th><th>95% CI</th></tr></thead><tbody><tr data-xml-align="center"><td data-xml-align="left">Alabama</td><td>117,000</td><td>(99,000, 137,000)</td><td>12.7</td><td>(10.7, 14.9)</td><td>112,000</td><td>(94,000, 132,000)</td><td>12.1</td><td>(10.2, 14.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Alaska</td><td>13,000</td><td>(11,000, 16,000)</td><td>8.6</td><td>(7.0, 10.4)</td><td>12,000</td><td>(10,000, 15,000)</td><td>8.1</td><td>(6.5, 9.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Arizona</td><td>114,000</td><td>(93,000, 138,000)</td><td>8.2</td><td>(6.7, 10.0)</td><td>107,000</td><td>(86,000, 130,000)</td><td>7.7</td><td>(6.2, 9.4)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Arkansas</td><td>76,000</td><td>(65,000, 89,000)</td><td>12.8</td><td>(10.9, 14.9)</td><td>63,000</td><td>(53,000, 73,000)</td><td>10.6</td><td>(9.0, 12.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">California</td><td>464,000</td><td>(365,000, 586,000)</td><td>6.1</td><td>(4.8, 7.7)</td><td>403,000</td><td>(304,000, 510,000)</td><td>5.3</td><td>(4.1, 6.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Colorado</td><td>93,000</td><td>(75,000, 112,000)</td><td>8.9</td><td>(7.2, 10.8)</td><td>80,000</td><td>(64,000, 99,000)</td><td>7.7</td><td>(6.2, 9.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Connecticut</td><td>74,000</td><td>(62,000, 87,000)</td><td>11.6</td><td>(9.7, 13.6)</td><td>68,000</td><td>(57,000, 81,000)</td><td>10.7</td><td>(8.9, 12.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Delaware</td><td>21,000</td><td>(18,000, 24,000)</td><td>12.1</td><td>(10.3, 14.1)</td><td>19,000</td><td>(16,000, 22,000)</td><td>11.1</td><td>(9.4, 13.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Florida</td><td>334,000</td><td>(277,000, 400,000)</td><td>9.5</td><td>(7.9, 11.4)</td><td>313,000</td><td>(256,000, 379,000)</td><td>8.9</td><td>(7.3, 10.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Georgia</td><td>274,000</td><td>(235,000, 315,000)</td><td>12.7</td><td>(10.9, 14.6)</td><td>242,000</td><td>(205,000, 280,000)</td><td>11.2</td><td>(9.6, 13.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Hawaii</td><td>17,000</td><td>(13,000, 20,000)</td><td>6.6</td><td>(5.2, 8.1)</td><td>15,000</td><td>(11,000, 18,000)</td><td>5.8</td><td>(4.5, 7.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Idaho</td><td>31,000</td><td>(26,000, 37,000)</td><td>8.6</td><td>(7.2, 10.2)</td><td>29,000</td><td>(24,000, 35,000)</td><td>8.0</td><td>(6.6, 9.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Illinois</td><td>226,000</td><td>(188,000, 270,000)</td><td>9.3</td><td>(7.7, 11.1)</td><td>205,000</td><td>(168,000, 248,000)</td><td>8.4</td><td>(6.9, 10.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Indiana</td><td>153,000</td><td>(131,000, 177,000)</td><td>11.6</td><td>(9.9, 13.4)</td><td>133,000</td><td>(114,000, 157,000)</td><td>10.2</td><td>(8.6, 11.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Iowa</td><td>63,000</td><td>(53,000, 73,000)</td><td>10.5</td><td>(8.8, 12.3)</td><td>59,000</td><td>(49,000, 69,000)</td><td>9.8</td><td>(8.2, 11.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Kansas</td><td>64,000</td><td>(53,000, 75,000)</td><td>10.7</td><td>(9.0, 12.6)</td><td>58,000</td><td>(48,000, 69,000)</td><td>9.8</td><td>(8.1, 11.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Kentucky</td><td>110,000</td><td>(94,000, 129,000)</td><td>13.2</td><td>(11.3, 15.4)</td><td>100,000</td><td>(84,000, 117,000)</td><td>12.0</td><td>(10.2, 14.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Louisiana</td><td>150,000</td><td>(129,000, 171,000)</td><td>16.3</td><td>(14.1, 18.6)</td><td>128,000</td><td>(110,000, 147,000)</td><td>14.0</td><td>(12.1, 16.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Maine</td><td>26,000</td><td>(22,000, 30,000)</td><td>12.1</td><td>(10.4, 13.9)</td><td>23,000</td><td>(20,000, 27,000)</td><td>11.1</td><td>(9.5, 12.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Maryland</td><td>121,000</td><td>(102,000, 143,000)</td><td>10.6</td><td>(8.9, 12.5)</td><td>113,000</td><td>(95,000, 135,000)</td><td>9.9</td><td>(8.3, 11.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Massachusetts</td><td>123,000</td><td>(104,000, 145,000)</td><td>10.7</td><td>(9.0, 12.6)</td><td>118,000</td><td>(98,000, 140,000)</td><td>10.2</td><td>(8.5, 12.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Michigan</td><td>197,000</td><td>(163,000, 236,000)</td><td>10.6</td><td>(8.8, 12.7)</td><td>180,000</td><td>(147,000, 217,000)</td><td>9.7</td><td>(7.9, 11.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Minnesota</td><td>93,000</td><td>(77,000, 112,000)</td><td>8.7</td><td>(7.2, 10.5)</td><td>85,000</td><td>(69,000, 103,000)</td><td>8.0</td><td>(6.5, 9.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Mississippi</td><td>96,000</td><td>(84,000, 111,000)</td><td>15.8</td><td>(13.7, 18.1)</td><td>88,000</td><td>(75,000, 102,000)</td><td>14.4</td><td>(12.4, 16.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Missouri</td><td>124,000</td><td>(105,000, 146,000)</td><td>10.9</td><td>(9.2, 12.8)</td><td>118,000</td><td>(98,000, 138,000)</td><td>10.3</td><td>(8.6, 12.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Montana</td><td>19,000</td><td>(16,000, 23,000)</td><td>10.0</td><td>(8.2, 12.2)</td><td>17,000</td><td>(14,000, 21,000)</td><td>9.1</td><td>(7.2, 11.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Nebraska</td><td>29,000</td><td>(23,000, 36,000)</td><td>7.5</td><td>(5.9, 9.3)</td><td>26,000</td><td>(21,000, 33,000)</td><td>6.8</td><td>(5.3, 8.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Nevada</td><td>36,000</td><td>(28,000, 44,000)</td><td>6.3</td><td>(5.0, 7.8)</td><td>32,000</td><td>(26,000, 41,000)</td><td>5.7</td><td>(4.5, 7.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Hampshire</td><td>26,000</td><td>(23,000, 30,000)</td><td>11.9</td><td>(10.2, 13.7)</td><td>25,000</td><td>(21,000, 29,000)</td><td>11.2</td><td>(9.6, 13.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Jersey</td><td>136,000</td><td>(110,000, 163,000)</td><td>7.9</td><td>(6.4, 9.5)</td><td>117,000</td><td>(93,000, 144,000)</td><td>6.8</td><td>(5.4, 8.4)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Mexico</td><td>34,000</td><td>(28,000, 42,000)</td><td>8.4</td><td>(6.8, 10.3)</td><td>31,000</td><td>(25,000, 38,000)</td><td>7.5</td><td>(6.0, 9.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New York</td><td>246,000</td><td>(198,000, 300,000)</td><td>7.2</td><td>(5.8, 8.8)</td><td>205,000</td><td>(164,000, 256,000)</td><td>6.0</td><td>(4.8, 7.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">North Carolina</td><td>235,000</td><td>(198,000, 276,000)</td><td>12.1</td><td>(10.2, 14.2)</td><td>222,000</td><td>(185,000, 263,000)</td><td>11.4</td><td>(9.6, 13.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">North Dakota</td><td>13,000</td><td>(10,000, 15,000)</td><td>9.0</td><td>(7.4, 10.8)</td><td>12,000</td><td>(9,000, 14,000)</td><td>8.3</td><td>(6.8, 10.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Ohio</td><td>256,000</td><td>(216,000, 302,000)</td><td>11.6</td><td>(9.8, 13.7)</td><td>227,000</td><td>(192,000, 269,000)</td><td>10.4</td><td>(8.7, 12.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Oklahoma</td><td>94,000</td><td>(78,000, 109,000)</td><td>11.7</td><td>(9.8, 13.7)</td><td>86,000</td><td>(72,000, 102,000)</td><td>10.8</td><td>(9.0, 12.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Oregon</td><td>66,000</td><td>(54,000, 80,000)</td><td>9.1</td><td>(7.4, 11.1)</td><td>63,000</td><td>(51,000, 77,000)</td><td>8.7</td><td>(7.0, 10.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Pennsylvania</td><td>204,000</td><td>(168,000, 244,000)</td><td>9.1</td><td>(7.5, 10.9)</td><td>179,000</td><td>(146,000, 215,000)</td><td>8.0</td><td>(6.6, 9.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Rhode Island</td><td>19,000</td><td>(16,000, 23,000)</td><td>11.3</td><td>(9.6, 13.3)</td><td>18,000</td><td>(15,000, 21,000)</td><td>10.6</td><td>(8.9, 12.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">South Carolina</td><td>117,000</td><td>(100,000, 135,000)</td><td>12.7</td><td>(10.9, 14.6)</td><td>102,000</td><td>(87,000, 118,000)</td><td>11.1</td><td>(9.5, 12.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">South Dakota</td><td>13,000</td><td>(11,000, 16,000)</td><td>7.5</td><td>(6.1, 9.1)</td><td>12,000</td><td>(9,000, 14,000)</td><td>6.7</td><td>(5.5, 8.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Tennessee</td><td>156,000</td><td>(131,000, 182,000)</td><td>12.5</td><td>(10.5, 14.6)</td><td>128,000</td><td>(107,000, 152,000)</td><td>10.4</td><td>(8.7, 12.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Texas</td><td>620,000</td><td>(516,000, 737,000)</td><td>10.1</td><td>(8.4, 12.0)</td><td>540,000</td><td>(442,000, 644,000)</td><td>8.8</td><td>(7.3, 10.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Utah</td><td>77,000</td><td>(63,000, 93,000)</td><td>10.0</td><td>(8.2, 12.0)</td><td>72,000</td><td>(58,000, 87,000)</td><td>9.3</td><td>(7.5, 11.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Vermont</td><td>10,000</td><td>(9,000, 13,000)</td><td>10.6</td><td>(8.7, 12.8)</td><td>9,000</td><td>(7,000, 11,000)</td><td>9.1</td><td>(7.3, 11.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Virginia</td><td>171,000</td><td>(144,000, 201,000)</td><td>10.8</td><td>(9.1, 12.7)</td><td>146,000</td><td>(120,000, 173,000)</td><td>9.2</td><td>(7.6, 11.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Washington</td><td>115,000</td><td>(96,000, 137,000)</td><td>8.3</td><td>(6.9, 9.9)</td><td>98,000</td><td>(80,000, 118,000)</td><td>7.1</td><td>(5.9, 8.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">West Virginia</td><td>45,000</td><td>(39,000, 52,000)</td><td>14.6</td><td>(12.5, 16.8)</td><td>40,000</td><td>(34,000, 47,000)</td><td>12.9</td><td>(11.0, 15.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Wisconsin</td><td>101,000</td><td>(83,000, 121,000)</td><td>9.5</td><td>(7.8, 11.4)</td><td>88,000</td><td>(71,000, 106,000)</td><td>8.3</td><td>(6.8, 10.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Wyoming</td><td>10,000</td><td>(8,000, 12,000)</td><td>8.7</td><td>(7.2, 10.4)</td><td>9,000</td><td>(7,000, 11,000)</td><td>8.0</td><td>(6.5, 9.7)</td></tr></tbody></table></div>
"""

html_code2 = """
<div class="table-wrap"><table><thead><tr><td>&nbsp;</td><th colspan="2"><span>Currently taking ADHD medication</span></th><th colspan="2"><span>Received behavioral treatment for ADHD in past 12 months</span></th><th colspan="2"><span>Received medication and/or behavioral treatment</span></th></tr><tr><td>&nbsp;</td><th>Weighted %</th><th>95% confidence interval</th><th>Weighted %</th><th>95% confidence interval</th><th>Weighted %</th><th>95% confidence interval</th></tr></thead><tbody><tr data-xml-align="center"><td data-xml-align="left">Alabama</td><td>77.0</td><td>(69.5, 83.4)</td><td>50.6</td><td>(41.2, 59.9)</td><td>84.3</td><td>(77.5, 89.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Alaska</td><td>61.4</td><td>(51.0, 71.1)</td><td>59.3</td><td>(48.6, 69.4)</td><td>79.4</td><td>(70.6, 86.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Arizona</td><td>55.2</td><td>(43.8, 66.1)</td><td>51.1</td><td>(40.2, 61.9)</td><td>71.7</td><td>(59.3, 82.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Arkansas</td><td>62.4</td><td>(54.0, 70.2)</td><td>46.5</td><td>(38.3, 54.8)</td><td>75.6</td><td>(67.8, 82.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">California</td><td>46.4</td><td>(33.6, 59.5)</td><td>49.2</td><td>(36.2, 62.2)</td><td>72.4</td><td>(58.7, 83.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Colorado</td><td>55.9</td><td>(44.3, 67.0)</td><td>43.0</td><td>(32.2, 54.3)</td><td>69.8</td><td>(57.5, 80.4)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Connecticut</td><td>56.9</td><td>(47.4, 66.0)</td><td>54.2</td><td>(44.9, 63.2)</td><td>79.1</td><td>(71.5, 85.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Delaware</td><td>75.4</td><td>(67.6, 82.1)</td><td>51.7</td><td>(43.0, 60.5)</td><td>85.6</td><td>(78.7, 90.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Florida</td><td>65.1</td><td>(54.2, 75.0)</td><td>49.2</td><td>(38.9, 59.5)</td><td>75.3</td><td>(63.8, 84.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Georgia</td><td>69.5</td><td>(61.3, 76.8)</td><td>43.2</td><td>(35.1, 51.5)</td><td>79.5</td><td>(71.7, 85.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Hawaii</td><td>52.8</td><td>(40.4, 65.0)</td><td>55.7</td><td>(43.5, 67.4)</td><td>75.7</td><td>(63.7, 85.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Idaho</td><td>58.7</td><td>(49.0, 68.0)</td><td>51.8</td><td>(41.9, 61.5)</td><td>74.7</td><td>(66.4, 81.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Illinois</td><td>64.6</td><td>(54.0, 74.3)</td><td>58.9</td><td>(49.0, 68.2)</td><td>82.3</td><td>(74.9, 88.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Indiana</td><td>64.4</td><td>(55.7, 72.4)</td><td>49.9</td><td>(41.4, 58.5)</td><td>79.8</td><td>(72.6, 85.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Iowa</td><td>65.4</td><td>(56.0, 74.0)</td><td>50.9</td><td>(41.6, 60.2)</td><td>78.3</td><td>(69.4, 85.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Kansas</td><td>62.8</td><td>(52.8, 72.0)</td><td>42.0</td><td>(33.0, 51.4)</td><td>75.3</td><td>(65.0, 83.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Kentucky</td><td>73.7</td><td>(65.8, 80.7)</td><td>48.8</td><td>(40.1, 57.5)</td><td>84.0</td><td>(76.4, 89.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Louisiana</td><td>73.9</td><td>(67.3, 79.9)</td><td>42.1</td><td>(34.7, 49.8)</td><td>82.9</td><td>(77.1, 87.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Maine</td><td>61.0</td><td>(53.1, 68.4)</td><td>45.4</td><td>(37.6, 53.4)</td><td>76.9</td><td>(69.7, 83.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Maryland</td><td>68.5</td><td>(59.9, 76.3)</td><td>56.0</td><td>(46.7, 65.1)</td><td>83.9</td><td>(76.7, 89.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Massachusetts</td><td>57.9</td><td>(48.1, 67.3)</td><td>50.0</td><td>(40.5, 59.5)</td><td>76.4</td><td>(67.5, 83.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Michigan</td><td>63.4</td><td>(52.5, 73.4)</td><td>44.8</td><td>(34.5, 55.5)</td><td>77.0</td><td>(67.3, 84.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Minnesota</td><td>64.9</td><td>(53.2, 75.3)</td><td>59.2</td><td>(49.4, 68.6)</td><td>87.6</td><td>(81.5, 92.2)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Mississippi</td><td>73.8</td><td>(65.6, 81.0)</td><td>50.1</td><td>(41.9, 58.4)</td><td>82.3</td><td>(74.5, 88.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Missouri</td><td>62.8</td><td>(53.4, 71.6)</td><td>45.2</td><td>(36.1, 54.5)</td><td>70.2</td><td>(60.6, 78.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Montana</td><td>65.6</td><td>(54.5, 75.5)</td><td>56.8</td><td>(45.3, 67.7)</td><td>77.3</td><td>(66.6, 85.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Nebraska</td><td>81.4</td><td>(72.7, 88.3)</td><td>61.8</td><td>(49.7, 72.9)</td><td>91.6</td><td>(85.8, 95.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Nevada</td><td>37.8</td><td>(27.1, 49.3)</td><td>39.9</td><td>(29.0, 51.7)</td><td>58.8</td><td>(46.0, 70.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Hampshire</td><td>65.4</td><td>(57.9, 72.4)</td><td>50.8</td><td>(42.7, 58.9)</td><td>78.9</td><td>(72.4, 84.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Jersey</td><td>41.1</td><td>(30.9, 51.9)</td><td>41.7</td><td>(31.1, 52.9)</td><td>58.4</td><td>(46.1, 69.9)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New Mexico</td><td>62.1</td><td>(50.8, 72.5)</td><td>47.8</td><td>(36.4, 59.4)</td><td>73.7</td><td>(63.5, 82.4)</td></tr><tr data-xml-align="center"><td data-xml-align="left">New York</td><td>54.8</td><td>(43.2, 66.1)</td><td>45.5</td><td>(34.7, 56.7)</td><td>70.5</td><td>(57.5, 81.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">North Carolina</td><td>64.5</td><td>(55.1, 73.1)</td><td>46.2</td><td>(37.0, 55.6)</td><td>78.8</td><td>(70.4, 85.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">North Dakota</td><td>64.0</td><td>(52.9, 74.2)</td><td>50.7</td><td>(40.4, 61.0)</td><td>78.0</td><td>(68.0, 86.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Ohio</td><td>58.1</td><td>(49.0, 66.9)</td><td>43.5</td><td>(34.7, 52.6)</td><td>75.1</td><td>(66.8, 82.3)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Oklahoma</td><td>66.1</td><td>(55.9, 75.3)</td><td>47.9</td><td>(38.5, 57.4)</td><td>79.0</td><td>(70.5, 86.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Oregon</td><td>50.2</td><td>(39.1, 61.2)</td><td>51.1</td><td>(40.1, 62.1)</td><td>70.2</td><td>(59.4, 79.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Pennsylvania</td><td>53.9</td><td>(43.7, 63.9)</td><td>54.4</td><td>(44.4, 64.2)</td><td>77.7</td><td>(68.2, 85.4)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Rhode Island</td><td>65.8</td><td>(57.1, 73.7)</td><td>48.3</td><td>(39.3, 57.3)</td><td>76.9</td><td>(68.8, 83.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">South Carolina</td><td>64.2</td><td>(56.1, 71.9)</td><td>45.0</td><td>(36.8, 53.4)</td><td>74.1</td><td>(66.2, 81.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">South Dakota</td><td>70.4</td><td>(60.7, 78.9)</td><td>45.2</td><td>(34.7, 56.2)</td><td>76.9</td><td>(67.5, 84.7)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Tennessee</td><td>62.8</td><td>(53.4, 71.5)</td><td>38.8</td><td>(30.2, 47.9)</td><td>74.1</td><td>(64.8, 82.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Texas</td><td>71.8</td><td>(63.5, 79.1)</td><td>40.5</td><td>(31.2, 50.3)</td><td>78.4</td><td>(70.5, 85.0)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Utah</td><td>59.5</td><td>(48.0, 70.4)</td><td>40.9</td><td>(30.7, 51.6)</td><td>72.2</td><td>(60.8, 81.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Vermont</td><td>52.0</td><td>(40.7, 63.2)</td><td>41.1</td><td>(30.2, 52.6)</td><td>65.0</td><td>(53.2, 75.6)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Virginia</td><td>62.7</td><td>(52.5, 72.1)</td><td>45.8</td><td>(36.5, 55.3)</td><td>76.9</td><td>(66.7, 85.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Washington</td><td>47.9</td><td>(38.5, 57.4)</td><td>39.2</td><td>(30.1, 48.9)</td><td>61.5</td><td>(51.8, 70.5)</td></tr><tr data-xml-align="center"><td data-xml-align="left">West Virginia</td><td>67.5</td><td>(59.1, 75.0)</td><td>46.5</td><td>(38.0, 55.2)</td><td>76.6</td><td>(69.0, 83.1)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Wisconsin</td><td>69.8</td><td>(58.3, 79.7)</td><td>51.3</td><td>(40.7, 61.8)</td><td>81.4</td><td>(70.0, 89.8)</td></tr><tr data-xml-align="center"><td data-xml-align="left">Wyoming</td><td>59.8</td><td>(49.3, 69.7)</td><td>54.3</td><td>(43.8, 64.5)</td><td>78.9</td><td>(69.3, 86.6)</td></tr></tbody></table></div>
"""

# Parse the HTML content with BeautifulSoup
soup1 = BeautifulSoup(html_code1, 'html.parser')

# Find the table element
table1 = soup1.find('table')

# Initialize lists to store data
data1 = []

# Extract table headers
headers = [th.text.strip() for th in table1.select('thead th')]

# Extract table rows
rows = table1.select('tbody tr')

# Iterate through each row
for row in rows:
    state_data = {}

    # Extract state name
    state_data['State'] = row.select_one('[data-xml-align="left"]').text.strip()

    # Extract weighted population estimates for both categories
    columns = row.select('td')[1:]
    for i in range(0, len(columns), 4):
        header_index = i // 4
        header = headers[header_index]
        value = int(re.sub(r'[^\d]', '', columns[i].text).replace(',', '_'))  # Convert to int without commas
        state_data[f'{header} Estimate'] = value

    # Append state data to the list
    data1.append(state_data)


# Create JSON
json_data1 = json.dumps(data1, indent=2)

soup2 = BeautifulSoup(html_code2, 'html.parser')

data2 = []

# Find the table body
table2 = soup2.find('tbody')

# Find all rows in the table body
rows = table2.find_all('tr')

for row in rows:
    state_data = {}
    columns = row.find_all('td')
    
    # Check if there are at least 6 columns (including the 'State' column)
    if len(columns) >= 6:
        state_name = columns[0].text.strip()
        state_data['State'] = state_name

        # Convert percentages to integers
        state_data['Currently taking ADHD medication'] = float(columns[1].text.strip())
        state_data['Received behavioral treatment for ADHD in past 12 months'] = float(columns[3].text.strip())

        data2.append(state_data)

# Convert data to JSON format
json_data2 = json.dumps(data2, indent=2)


url = "https://www.britannica.com/topic/largest-U-S-state-by-population"

# Send a GET request to the URL
response = requests.get(url)

# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Parse the HTML content of the page
    soup3 = BeautifulSoup(response.text, 'html.parser')

    # Find the table containing state data
    table3 = soup3.find('table')

    # Initialize an empty list to store state data
    state_data = []

    # Extract data from each row in the table
    for row in table3.find_all('tr')[1:]:  # Skip the header row
        columns = row.find_all('td')

        # Extract relevant information
        name = columns[0].text.split('.', 1)[1].strip()  # Remove the state number
        total_population_str = columns[1].text.replace('(2023 est.)', '').replace(',', '').strip()  # Remove "(2023 est.)" and commas
        total_population = int(total_population_str)  # Convert to integer

        # Create a dictionary for each state
        state_info = {
            'State': name,
            'Total Population': total_population
        }

        # Add the dictionary to the list
        state_data.append(state_info)

    # Create a JSON representation of the state data
    json_data3 = json.dumps(state_data, indent=2)

else:
    print(f"Failed to retrieve the page. Status code: {response.status_code}")

data1 = json.loads(json_data1)
data2 = json.loads(json_data2)
data3 = json.loads(json_data3)

combined_data = []

for entry1 in data1:
    state = entry1["State"]
    entry2 = next((entry for entry in data2 if entry["State"] == state), None)
    entry3 = next((entry for entry in data3 if entry["State"] == state), None)

    if entry2 and entry3:
        combined_entry = {
            "State": state,
            "Total Population": entry3["Total Population"],
            "Ever received ADHD diagnosis": entry1["Ever received ADHD diagnosis Estimate"],
            "Has current ADHD": entry1["Has current ADHD Estimate"],
            "Currently taking ADHD medication": entry2["Currently taking ADHD medication"],
            "Received behavioral treatment for ADHD": entry2["Received behavioral treatment for ADHD in past 12 months"]
        }
        combined_data.append(combined_entry)

combined_json = json.dumps(combined_data, indent=2)

# Save JSON to a file or print it
with open('states_data.json', 'w') as json_file:
    json_file.write(combined_json)
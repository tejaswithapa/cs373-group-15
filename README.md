# ADHD Ally US - Group 15

### Team Member Information

| Name               | GitLab ID        | EID     |
|:------------------ | ---------------- | ------- |
| Abdallah Al-Sukhni | @doogls          | aha2242 |
| Naman Arora        | @naman-arora     | na26896 |
| Haoyu Fan          | @haoyujfan       | hf5542  |
| Faith Nguyen       | @fpnguyen        | pnn329  |
| Tejaswi Thapa      | @tejaswithapa    | tt26788 |

### Website

[ADHD Ally](https://adhdallyus.site/)

### Backend API

[ADHD Ally API](https://api.adhdallyus.site/)

### API Documentation

[Postman](https://documenter.getpostman.com/view/32958006/2sA2r53kYu)

### PechaKucha Presentation

[YouTube](https://www.youtube.com/watch?v=QJsGw3L5wac&ab_channel=AbdullahAl-Sukhni)

### Estimated/Actual Completion Time

| Phase | Member             | Estimated (hrs) | Actual (hrs) |
| ----- | ------------------ | --------------- | ------------ |
| 1     | Abdallah Al-Sukhni | 20              |   25         |
|       | Naman Arora        | 20              |   25         |
|       | Haoyu Fan          | 20              |   25         |
|       | Faith Nguyen       | 20              |   25         |
|       | Tejaswi Thapa      | 20              |   25         |
| 2     | Abdallah Al-Sukhni | 30              |   35         |
|       | Naman Arora        | 30              |   35         |
|       | Haoyu Fan          | 30              |   35         |
|       | Faith Nguyen       | 30              |   35         |
|       | Tejaswi Thapa      | 30              |   35         |
| 3     | Abdallah Al-Sukhni | 25              |   25         |
|       | Naman Arora        | 25              |   25         |
|       | Haoyu Fan          | 25              |   25         |
|       | Faith Nguyen       | 25              |   25         |
|       | Tejaswi Thapa      | 25              |   25         |
| 4     | Abdallah Al-Sukhni | 10              |   10         |
|       | Naman Arora        | 10              |   10         |
|       | Haoyu Fan          | 10              |   10         |
|       | Faith Nguyen       | 10              |   10         |
|       | Tejaswi Thapa      | 10              |   10         |

### Project Leader

| Phase |    Phase Leader                       |
| ----- | ------------------------------------- |
| 1     |   Faith Nguyen                        |
| 2     |   Naman Arora                         |
| 3     |   Haoyu Fan                           |
| 4     |   Tejaswi Thapa, Abdallah Al-Sukhni   |

**Phase Leader Responsibilities:** Effectively communciate with team members, set up meeting times, keep track of progress, and distribute work evenly.

### GitLab Pipelines

[Pipelines](https://gitlab.com/tejaswithapa/cs373-group-15/-/pipelines)

### Git SHA

| Phase |                 SHA                       |
| ----- | ----------------------------------------- |
| 1     | eb5faf1fb321d9a24876b185b201e9965b371594  |
| 2     | 93a85199197645fb8b4516e66ab002dd63456778  |
| 3     | f7c7c01bcc8f6af1c84b169188bf4a82e45f65a3  |
| 4     | 9d7bf48edf932ca7e258db8eaf9b3675600da95d  |

### Comments

import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


url = 'https://adhdallyus.site/'

class Test(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument("--start-maximized")
        chrome_options.add_argument("disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=chrome_options)

    def tearDown(self):
        self.driver.quit()

    # Acceptance Test 1
    def test1(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'About')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/about"))
        self.assertEqual(self.driver.current_url, url + "about")

    # Acceptance Test 2
    def test2(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'States')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states"))
        self.assertEqual(self.driver.current_url, url + "states")

    # Acceptance Test 3
    def test3(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'Professionals')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals"))
        self.assertEqual(self.driver.current_url, url + "professionals")

    # Acceptance Test 4
    def test4(self):
        self.driver.get(url)
        self.driver.implicitly_wait(20)
        button = self.driver.find_element(By.LINK_TEXT, 'Support Groups')
        button.click()
        WebDriverWait(self.driver, 20).until(EC.url_contains("/supportgroups"))
        self.assertEqual(self.driver.current_url, url + "supportgroups")

    # Acceptance Test 5
    def test_states_card(self):
        self.driver.get(url + 'states')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states/Alabama"))
        self.assertEqual(self.driver.current_url, url + "states/Alabama")

    # Acceptance Test 6
    def test_professionals_card(self):
        self.driver.get(url + 'professionals')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals/dcc4d16c-0293-427a-be81-3529449bec21"))
        self.assertEqual(self.driver.current_url, url + "professionals/dcc4d16c-0293-427a-be81-3529449bec21")

    # Acceptance Test 7
    def test_supportgroups_card(self):
        self.driver.get(url + 'supportgroups')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/supportgroups/9068197b-0db9-44d2-8c51-df32dc4ae966"))
        self.assertEqual(self.driver.current_url, url + "supportgroups/9068197b-0db9-44d2-8c51-df32dc4ae966")
        
    # Acceptance Test 8
    def test_states_home(self):
        self.driver.get(url)
        # self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        self.driver.implicitly_wait(30)
        ActionChains(self.driver).send_keys(Keys.END).perform()
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/section[3]/div/div/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states"))
        self.assertEqual(self.driver.current_url, url + "states")
  
    # Acceptance Test 9      
    def test_profs_home(self):
        self.driver.get(url)
        self.driver.implicitly_wait(30)
        # self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        ActionChains(self.driver).send_keys(Keys.END).perform()
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/main/section[4]/div/div/div[2]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals"))
        self.assertEqual(self.driver.current_url, url + "professionals")

    # Acceptance Test 10
    def test_home_navbar(self):
        self.driver.get(url + 'states')
        self.driver.implicitly_wait(20)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.LINK_TEXT, 'ADHDAlly')))
        button = self.driver.find_element(By.LINK_TEXT, 'ADHDAlly')
        # self.driver.execute_script("arguments[0].scrollIntoView();", button)
        # self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.invisibility_of_element_located((By.LINK_TEXT, 'states')))
        self.assertEqual(self.driver.current_url, url)
        
    # Search, Sort, Filter
    # Acceptance Test 11
    def test_search_states(self):
        self.driver.get(url + 'states?order=desc&search=texas')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/states/Texas"))
        self.assertEqual(self.driver.current_url, url + "states/Texas")

    # Acceptance Test 12
    def test_search_sort_profs(self):
        self.driver.get(url + 'professionals?order=desc&search=texas')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/professionals/0995525d-edbd-494b-b2a6-29ee5f70d711"))
        self.assertEqual(self.driver.current_url, url + "professionals/0995525d-edbd-494b-b2a6-29ee5f70d711")
        
    # Acceptance Test 13
    def test_search_support_groups(self):
        self.driver.get(url + 'supportgroups?search=texas')
        self.driver.implicitly_wait(20)
        button = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, '/html/body/section[2]/div[1]/div[2]/a')))
        self.driver.execute_script("arguments[0].scrollIntoView();", button)
        self.driver.implicitly_wait(20)
        self.driver.execute_script("arguments[0].click();", button)
        WebDriverWait(self.driver, 20).until(EC.url_contains("/supportgroups/42879b1f-5d1a-4969-9af9-f672b7be3106"))
        self.assertEqual(self.driver.current_url, url + "supportgroups/42879b1f-5d1a-4969-9af9-f672b7be3106")
        
    




if __name__ == "__main__":
    unittest.main()
